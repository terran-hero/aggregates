﻿[assembly: System.Reflection.AssemblyTitle("Basic.SqlServer")]
[assembly: System.Reflection.AssemblyDescription("金软科技SqlServer数据库扩展程序集")]
[assembly: System.Reflection.AssemblyProduct("Basic.SqlServer")]
[assembly: System.Reflection.AssemblyAlgorithmId(System.Configuration.Assemblies.AssemblyHashAlgorithm.MD5)]
[assembly: System.Runtime.InteropServices.ComVisible(false)]
[assembly: System.Runtime.InteropServices.Guid("425DD88E-2720-4510-856C-0DE7DBB789CA")]
[assembly: System.Reflection.AssemblyCompany("SIP GoldSoft Technology Limited")]
[assembly: System.Reflection.AssemblyCopyright("@2003 - 2023 GoldSoft Technology, All Rights Reserved.")]
[assembly: System.Reflection.AssemblyTrademark("Basic for Sql Server")]
[assembly: System.Reflection.AssemblyConfiguration("Release")]
#if NET472
[assembly: System.Reflection.AssemblyVersion("4.7.2.0")]
[assembly: System.Reflection.AssemblyFileVersionAttribute("4.7.2.0")]
[assembly: System.Security.SecurityRules(System.Security.SecurityRuleSet.Level2, SkipVerificationInFullTrust = true)]
#elif NET45
[assembly: System.Reflection.AssemblyVersion("4.5.0.0")]
[assembly: System.Reflection.AssemblyFileVersionAttribute("4.5.0.0")]
[assembly: System.Security.SecurityRules(System.Security.SecurityRuleSet.Level2, SkipVerificationInFullTrust = true)]
#elif NET40
[assembly: System.Reflection.AssemblyVersion("4.0.0.0")]
[assembly: System.Reflection.AssemblyFileVersionAttribute("4.0.0.0")]
[assembly: System.Security.SecurityRules(System.Security.SecurityRuleSet.Level2, SkipVerificationInFullTrust = true)]
#elif NET35
[assembly: System.Reflection.AssemblyVersion("3.5.0.0")]
[assembly: System.Reflection.AssemblyFileVersionAttribute("3.5.0.0")]
#endif
